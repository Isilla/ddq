﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    [SerializeField]
    private Slider slider;
    [SerializeField]
    private TextMeshProUGUI timerText;
    [SerializeField]
    private TextMeshProUGUI scoreText;
    [SerializeField]
    private GameObject bonusObj;
    [SerializeField]
    private EnemySpawner spawnerObj;
    [SerializeField]
    private GameObject gameoverUI;
    [SerializeField]
    private int timerValueDefault;
    [SerializeField]
    private int scoreValueDefault;


    private static int timerValue = 30;
    public static int TimerValue
    {
        get { return timerValue; }
    }

    private static int scoreValue = 10;
    public static int ScoreValue
    {
        get { return scoreValue; }
    }

    private static int maxSliderValue = 5;
    private static int sliderValue = 0;
    private Coroutine timerCoroutine;

    public enum GState
    {
        Start,
        Action,
        Pause,
        GameOver
    }
    private static GState gameState = GState.Start;
    public static GState GameState
    {
        get { return gameState; }
    }
    void Start()
    {
        gameState = GState.Start;
        timerText.text = timerValue.ToString();
        scoreText.text = scoreValue.ToString();
        if (scoreValueDefault <= 0)
        {
            scoreValueDefault = 10;
        }
        if (timerValueDefault <= 0)
        {
            timerValueDefault = 30;
        }
    }

    private void Update()
    {
        UpdateUI();
        if(isOverSlider())
        {
            sliderValue = 0;
            var obj = Instantiate(bonusObj);
            float xPos = UnityEngine.Random.Range(-GameCamera.HalfWidth, GameCamera.HalfWidth);
            float yPos = UnityEngine.Random.Range(-GameCamera.HalfHeight, GameCamera.HalfHeight);
            obj.transform.position = new Vector2(xPos, yPos);
        }
        if(scoreValue <= 0)
        {
            GameOver();
        }
    }

    private static bool isOverSlider()
    {
        if (sliderValue >= maxSliderValue)
        {
            return true;
        }
        return false;
    }

    public static void IncreaceTime(int value)
    {
        if (gameState == GState.Action)
        {
            timerValue += value;
            sliderValue = 0;
        }
    }
    public static void IncreaceScore(int value)
    {
        if(gameState == GState.Action)
        {
            scoreValue += value;
            sliderValue++;
            if(value < 0)
            {
                sliderValue = 0;
            }
        }
    }

    public void UpdateUI()
    {
        timerText.text = timerValue.ToString();
        scoreText.text = scoreValue.ToString();
        slider.value = (float)sliderValue / maxSliderValue;
    }

    private IEnumerator Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            timerValue--;
            if(timerValue <= 0)
            {
                GameOver();
            }
            UpdateUI();
        }
    }
    private void GameOver()
    {
        StopCoroutine(timerCoroutine);
        spawnerObj.StopSpawn();
        gameState = GState.GameOver;
        gameoverUI.SetActive(true);

        var bonuses = GameObject.FindGameObjectsWithTag("Bonus");
        foreach (var bonus in bonuses)
        {
            Destroy(bonus);
        }
    }
    public void RestartGame()
    {
        slider.value = 0f;
        timerCoroutine = StartCoroutine(Timer());
        spawnerObj.StartSpawn();
        scoreValue = scoreValueDefault;
        timerValue = timerValueDefault;
        gameState = GState.Action;
    }
}
