﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private static float halfHeight = 0;
    private static float halfWidth = 0;
    public static float HalfHeight
    {
        get
        {
            if (halfHeight == 0)
            {
                halfHeight = Camera.main.orthographicSize;
            }
            return halfHeight;
        }
    }
    public static float HalfWidth
    {
        get
        {
            if (halfWidth == 0)
            {
                Camera c = Camera.main;
                halfWidth = c.aspect * c.orthographicSize;
            }
            return halfWidth;
        }
    }
}
