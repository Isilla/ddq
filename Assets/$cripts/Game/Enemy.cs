﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private EnemyData enemyData;
    private GameObject enemyObj;
    private bool isAlive = true;
    private int HP = 10;
    private Animator animator;
    private Rigidbody2D rb;
    public void Init(EnemyData dt)
    {
        enemyData = dt;
        foreach (Transform child in transform)
        {
            if (child.tag == "Enemy")
            {
                Destroy(child.gameObject);
            }
        }
        isAlive = true;
        enemyObj = Instantiate(dt.MainObject);
        enemyObj.transform.parent = transform;
        enemyObj.transform.position = transform.position;
        animator = enemyObj.GetComponent<Animator>();
        HP = dt.HP.GetValue();
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public static Action<GameObject> OnEnemyOverFly;

    void Update()
    {
        transform.Translate(Vector3.left * enemyData.Speed * Time.deltaTime);
        if(transform.position.x < -(GameCamera.HalfWidth + 1) && OnEnemyOverFly != null && isAlive) 
        {
            OnEnemyOverFly(gameObject);
            Manager.IncreaceScore(-enemyData.ScoreResiver);
        }
    }

    private void OnMouseDown()
    {
        HP--;
        animator.SetTrigger("isHitting");
        if (HP <= 0 && isAlive)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            Invoke(nameof(DeleteObject), 3);
            isAlive = false;
            animator.SetBool("isDead", true);
            Manager.IncreaceScore(enemyData.ScoreTaker);
        }
    }
    private void DeleteObject()
    {
        rb.constraints = RigidbodyConstraints2D.FreezePositionY;
        OnEnemyOverFly(gameObject);
    }
}
