﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyData : ScriptableObject
{
    [SerializeField]
    private GameObject mainObject;
    public GameObject MainObject
    {
        get { return mainObject; }
    }

    [SerializeField]
    private FloatMinMax speed;
    public float Speed
    {
        get
        {
            return speed.GetValue();
        }
    }

    [SerializeField]
    private int scoreResiver;
    public int ScoreResiver
    {
        get { return scoreResiver; }
    }

    [SerializeField]
    private int scoreTaker;
    public int ScoreTaker
    {
        get { return scoreTaker; }
    }
    [SerializeField]
    public IntMinMax HP;

}
