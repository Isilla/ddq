﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BonusTime : MonoBehaviour
{
    private void OnMouseDown()
    {
        Manager.IncreaceTime(3);
        Destroy(this.gameObject);
    }
}
