﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private List<EnemyData> enemyBlueprints;

    [SerializeField]
    private int maxEnemies;

    [SerializeField]
    private GameObject enemyPrefab;

    [SerializeField]
    private FloatMinMax spawnTime;

    public static Dictionary<GameObject, Enemy> enemies;

    private Queue<GameObject> currentEnemies;

    private Coroutine spawnCoroutine;


    private void Start()
    {
        enemies = new Dictionary<GameObject, Enemy>();
        currentEnemies = new Queue<GameObject>();
        for (int i=0;i<maxEnemies; i++)
        {
            var prefab = Instantiate(enemyPrefab);
            var script = prefab.GetComponent<Enemy>();
            prefab.SetActive(false);
            enemies.Add(prefab, script);
            currentEnemies.Enqueue(prefab);
        }
        Enemy.OnEnemyOverFly += ReturnEnemy;
    }

    public void StopSpawn()
    {
        StopCoroutine(spawnCoroutine);
    }
    public void StartSpawn()
    {
        spawnCoroutine = StartCoroutine(Spawn());
    }
    private IEnumerator Spawn()
    {
        float st = spawnTime.GetValue();
        if (st <= 0.1f)
        {
            st += 0.5f;
        }
        while(true)
        {
            yield return new WaitForSeconds(st);
            if (currentEnemies.Count > 0)
            {
                var enemy = currentEnemies.Dequeue();
                var script = enemies[enemy];
                enemy.SetActive(true);
                float yPos = UnityEngine.Random.Range(-GameCamera.HalfHeight, GameCamera.HalfHeight);
                enemy.transform.position = new Vector2(transform.position.x, yPos);
                int rand = UnityEngine.Random.Range(0, enemyBlueprints.Count);
                script.Init(enemyBlueprints[rand]);                
            }
        }
    }

    private void ReturnEnemy(GameObject _enemy)
    {
        _enemy.transform.position = transform.position;
        _enemy.SetActive(false);
        currentEnemies.Enqueue(_enemy);
    }
}
